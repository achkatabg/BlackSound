﻿using BlackSound.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BlackSound.Repositories
{
    public class PlaylistRepository:BaseRepository<Playlist>
    {
        public PlaylistRepository(string filePath) : base(filePath)
        {
        }
        protected override void PopulateEntity(StreamReader sr, Playlist item)
        {
            item.ID = int.Parse(sr.ReadLine());
            item.UserID = int.Parse(sr.ReadLine());
            item.Name = sr.ReadLine();
            item.Description = sr.ReadLine();
            item.IsPublic=Convert.ToBoolean(sr.ReadLine());

            //PlaylistsSongsRepository plsrepo = new PlaylistsSongsRepository();
            //plsrepo.GetAll(c => c.)

        }
        protected override void WriteEntity(StreamWriter sw, Playlist item)
        {
            sw.WriteLine(item.ID);
            sw.WriteLine(item.UserID);
            sw.WriteLine(item.Name);
            sw.WriteLine(item.Description);
            sw.WriteLine(item.IsPublic);
        }

        public override void Delete(Playlist item)
        {
            PlaylistsSongsRepository plsRepo = new PlaylistsSongsRepository("plsongs.txt");
            var playlistSongs = plsRepo.GetAll(x => x.PlaylistID == item.ID);

            foreach (var playlistSong in playlistSongs)
            {
                plsRepo.Delete(playlistSong);
            }

            base.Delete(item);
        }
    }
}

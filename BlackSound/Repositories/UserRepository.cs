﻿using BlackSound.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Repositories
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(string filePath) : base(filePath)
        {
        }
        protected override void PopulateEntity(StreamReader sr, User item)
        {
            item.ID = int.Parse(sr.ReadLine());
            item.NickName = sr.ReadLine();
            item.email = sr.ReadLine();
            item.password = sr.ReadLine();
            item.IsAdministrator = Convert.ToBoolean(sr.ReadLine());
        }

        protected override void WriteEntity(StreamWriter sw, User item)
        {
            sw.WriteLine(item.ID);
            sw.WriteLine(item.NickName);
            sw.WriteLine(item.email);
            sw.WriteLine(item.password);
            sw.WriteLine(item.IsAdministrator);
        }

        public User GetByEmailAndPassword(string email, string password)
        {
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);
            try
            {
                while (!sr.EndOfStream)
                {
                    User user = new User();
                    user.ID = Convert.ToInt32(sr.ReadLine());
                    user.NickName = sr.ReadLine();
                    user.email = sr.ReadLine();
                    user.password = sr.ReadLine();
                    user.IsAdministrator = Convert.ToBoolean(sr.ReadLine());
                    if (user.email == email && user.password == password)
                    {
                        return user;
                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }
            return null;
        }

        public override void Delete(User item)
        {
            PlaylistRepository playRepo = new PlaylistRepository("playlists.txt");
            List<Playlist> playlists = playRepo.GetAll(x => x.UserID == item.ID);

            foreach (var playlist in playlists)
            {
                playRepo.Delete(playlist);
            }

            base.Delete(item);  
        }
    }    
}


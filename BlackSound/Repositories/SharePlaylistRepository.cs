﻿using BlackSound.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Repositories
{
    public class SharePlaylistRepository : BaseRepository<SharedPlaylists>
    {
        public SharePlaylistRepository(string filePath) : base(filePath)
        {

        }
        protected override void PopulateEntity(StreamReader sr, SharedPlaylists item)
        {
            item.ID = int.Parse(sr.ReadLine());
            item.SharedFromUserID = int.Parse(sr.ReadLine());
            item.PlaylistID = int.Parse(sr.ReadLine());
            item.UserID = int.Parse(sr.ReadLine());
        }
        protected override void WriteEntity(StreamWriter sw, SharedPlaylists item)
        {
            sw.WriteLine(item.ID);
            sw.WriteLine(item.SharedFromUserID);
            sw.WriteLine(item.PlaylistID);
            sw.WriteLine(item.UserID);
        }
    }
}
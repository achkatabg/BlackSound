﻿using BlackSound.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BlackSound.Repositories
{
    public class SongRepository:BaseRepository<Song>
    {
        public SongRepository(string filePath) : base(filePath)
        {
        }
        protected override void PopulateEntity(StreamReader sr, Song item)
        {
            item.ID = int.Parse(sr.ReadLine());
            item.Title = sr.ReadLine();
            item.ArtistName = sr.ReadLine();
            item.Year = int.Parse(sr.ReadLine());
        }
        protected override void WriteEntity(StreamWriter sw, Song item)
        {
            sw.WriteLine(item.ID);
            sw.WriteLine(item.Title);
            sw.WriteLine(item.ArtistName);
            sw.WriteLine(item.Year);
        }
    }
}

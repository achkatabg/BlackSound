﻿using BlackSound.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Repositories
{
    public class RegisterRepository: BaseRepository<User>
    {
        public RegisterRepository(string filePath) : base(filePath)
        {
        }
        protected override void PopulateEntity(StreamReader sr, User item)
        {
            item.ID = int.Parse(sr.ReadLine());
            item.NickName =sr.ReadLine();
            item.email = sr.ReadLine();
            item.password = sr.ReadLine();
            item.IsAdministrator = Convert.ToBoolean(sr.ReadLine()); 
        }

        protected override void WriteEntity(StreamWriter sw, User item)
        {
            sw.WriteLine(item.ID);
            sw.WriteLine(item.NickName);
            sw.WriteLine(item.email);
            sw.WriteLine(item.password);
            sw.WriteLine(item.IsAdministrator);
        }

        public void Register (string nickname,string email,string password)
        {
            User user = new User() {
                NickName=nickname,
                email=email, 
                password=password
            };
            Save(user);
        }
    }
}

﻿using BlackSound.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using BlackSound.Helpers;

namespace BlackSound.Repositories
{
    public class PlaylistsSongsRepository : BaseRepository<PlaylistsSongs>
    {
        public PlaylistsSongsRepository(string filePath) : base(filePath)
        {
        }
        protected override void PopulateEntity(StreamReader sr, PlaylistsSongs item)
        {
            item.ID = int.Parse(sr.ReadLine());
            item.PlaylistID = int.Parse(sr.ReadLine());
            item.SongID = int.Parse(sr.ReadLine());
        }
        protected override void WriteEntity(StreamWriter sw, PlaylistsSongs item)
        {
            sw.WriteLine(item.ID);
            sw.WriteLine(item.PlaylistID);
            sw.WriteLine(item.SongID);
        }

        public override void Delete(PlaylistsSongs item)
        {
            SongRepository songRepo = new SongRepository("songs.txt");
            List<Song> songs = songRepo.GetAll(x => x.ID == item.SongID);
            foreach (var song in songs)
            {
                songRepo.Delete(song);
            }
            base.Delete(item);
        }
    }
}

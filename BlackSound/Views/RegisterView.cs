﻿using BlackSound.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSound.Repositories;
using System.IO;

namespace BlackSound.Views
{
    public class RegisterView
    {
        #region Public Register
        public void CheckUserExits(string nickname, string email)
        {
            List<string> fileLines = File.ReadLines("blacksoundusers.txt").ToList();

            foreach (var line in fileLines)
            {
                if (line.Equals(nickname) || line.Equals(email))
                {
                    Console.Clear();
                    Console.Write("This user already exists !");
                    Console.ReadKey(true);
                    Console.Clear();
                    new LoginView().Show();
                    
                }

            }
        }
        public void Show()
        {
            Console.Clear();
            Console.WriteLine("BlackSound> [REGISTER]\n");
            Console.Write("Nickname: ");
            string nickname = Console.ReadLine();
            Console.Write("Email: ");
            string email = Console.ReadLine();
            while (true)
            {
                Console.Write("Password: ");
                PasswordMask pm = new PasswordMask();
                string password = pm.ReadPassword();
                Console.Write("Confirm Password: ");
                string confirmpass = pm.ReadPassword();
                CheckUserExits(nickname, email);
                if(password==confirmpass)
                {
                    RegisterRepository reg = new RegisterRepository("blacksoundusers.txt");
                    reg.Register(nickname,email,password);
                    Console.Clear();
                    Console.Write("You have been successfully registered in BlackSound!");
                    Console.ReadKey(true);
                    break;
                }
                else
                {
                    Console.Clear();
                    Console.Write("Password's not same ...\n");
                    Console.ReadKey(true);
                    Console.Clear();
                }
            }
       }
        #endregion
    }
}

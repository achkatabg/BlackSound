﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSound.Helpers;

namespace BlackSound.Views
{
    public class GuestView
    {   
        #region Guest
        private string GuestGenerator()
        {   
            Random r = new Random();
            int gnumber = r.Next(0, 90000);
            string guestnum = "[Guest# " + gnumber + "]\n";
            return guestnum;
        }
        private GuestMenuEnum RenderGuestMenu()
        {
            string guestnumber = GuestGenerator();
            while (true)
            {     
                Console.Clear();
                Console.WriteLine("\n### Welcome to BlackSound v[0.1] ###\n");
                Console.WriteLine(guestnumber);
                Console.WriteLine("1.[L]ogin");
                Console.WriteLine("2.[R]egister\n");
                Console.WriteLine("----------------------\n");
                Console.WriteLine("E[x]it");

                Console.Write("\nBlackSound> ");
                string choice = Console.ReadLine();

                switch (choice.ToUpper())
                {
                    case "L":
                        {
                            return GuestMenuEnum.Login;
                        }
                    case "R":
                        {
                            return GuestMenuEnum.Register;
                        }
                    case "X":
                        {
                            return GuestMenuEnum.Exit;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }
                }
            }
        }
        public void Show()
        {
            while(true)
            {
                GuestMenuEnum choice = RenderGuestMenu();
                try
                {
                    switch(choice)
                    {
                        case GuestMenuEnum.Login:
                            {
                                new LoginView().Show();
                                break;
                            }
                        case GuestMenuEnum.Register:
                            {
                                new RegisterView().Show();
                                break;
                            }
                        case GuestMenuEnum.Exit:
                            {
                                Environment.Exit(0);
                                break;
                            }
                    }
                }
                catch (Exception ex)
                {
                    Console.Clear();
                    Console.WriteLine(ex.Message);
                    Console.ReadKey(true);
                }
            }
        }
        #endregion;
    }
}

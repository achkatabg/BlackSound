﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSound.Entities;
using BlackSound.Authentication;
using BlackSound.Helpers;
using BlackSound.Views;
using BlackSound.Repositories;


namespace BlackSound.Views
{
    public class UserMenuView
    {
        #region Repositories and etc.
        PlaylistRepository playRepo = new PlaylistRepository("playlists.txt");
        SongRepository songRepo = new SongRepository("songs.txt");
        PlaylistsSongsRepository plsRepo = new PlaylistsSongsRepository("plsongs.txt");
        UserRepository userRepo = new UserRepository("blacksoundusers.txt");
        SharePlaylistRepository shareRepo = new SharePlaylistRepository("sharedplaylists.txt");
        Playlist play = new Playlist();
        
        #endregion
        #region UserMainMenu
        #region CRUD
        private void EditPlaylist()
        {
            Console.Clear();
            Console.Write("*-- Edit Playlist\n");
            Console.Write("\nChoose PlaylistID: ");
            int playlistID = int.Parse(Console.ReadLine());
            Playlist play = playRepo.GetById(playlistID);
            if(play == null)
            {
                Console.Clear();
                Console.Write("Playlist not found!");
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Playlist found ...");
                Console.ReadKey(true);
                new UserMenuView().ShowPlaylistMng(play);
            } 
        }
        private void SharePlaylist()
        {
            Console.Clear();
            Console.WriteLine("*-- Share Playlist\n");
            List<Playlist> playlists = playRepo.GetAll(x => x.UserID == AuthenticationService.LoggedUser.ID);
            Console.WriteLine("================= [MY PLAYLIST'S] =================\n");
            foreach (Playlist item in playlists)
            {
                Console.WriteLine("-------------------------------------------------");
                Console.WriteLine($"| ID# {item.ID} | Playlist: {item.Name}");
                Console.WriteLine("-------------------------------------------------");

            }
            Console.WriteLine("\n================= [MY PLAYLIST'S] =================");
            List<User> users = userRepo.GetAll();
            Console.WriteLine("\n\n============================= USERS =============================");
            foreach (User item in users)
            {
                Console.WriteLine($"#ID {item.ID} | #NickName: {item.NickName}");
            }
            Console.WriteLine("============================= USERS =============================");

            Console.Write("\nChoose PlaylistID: ");
            int playlistID = int.Parse(Console.ReadLine());
            Playlist play = playRepo.GetById(playlistID);
            if (play == null)
            {
                Console.Clear();
                Console.WriteLine("Incorrect PlaylistID!");
                Console.ReadKey(true);
                new UserMenuView().Show();
            }
            Console.Write("\nChoose UserID: ");
            int userID = int.Parse(Console.ReadLine());
            User user = userRepo.GetById(userID);
            SharedPlaylists shareplay = new SharedPlaylists();
            if (user == null)
            {
                Console.Clear();
                Console.WriteLine("User not found with this ID ...");
                Console.ReadKey(true);
                new UserMenuView().Show();
            }
            else
            {
                shareplay.SharedFromUserID = AuthenticationService.LoggedUser.ID;
                shareplay.PlaylistID = playlistID;
                shareplay.UserID = userID;
                shareRepo.Save(shareplay);
                Console.Clear();
                Console.WriteLine("You share playlist successfully!");
                Console.ReadKey(true);
                new UserMenuView().Show();
            }
        }
        private void AddPlaylist()
        {
            Console.Clear();
            Console.Write("*--Add Playlist\n");
            Playlist play = new Playlist();
            play.UserID = AuthenticationService.LoggedUser.ID;
            Console.Write("\nPlaylistName: ");
            play.Name = Console.ReadLine();
            Console.Write("Description: ");
            play.Description = Console.ReadLine();
            Console.Write("Public(True/False): ");
            play.IsPublic = Convert.ToBoolean(Console.ReadLine());
            playRepo.Save(play);
            Console.Clear();
            Console.WriteLine("Playlist saved ...");
            Console.ReadKey(true);
            new UserMenuView().Show();
        }
        public void GetAllPublicPlaylists()
        {
            List<User> users = userRepo.GetAll();
            List<Playlist> playlists = playRepo.GetAll(x => x.IsPublic == true && x.UserID!=AuthenticationService.LoggedUser.ID);
            if (playlists.Count > 0)
            {
                Console.WriteLine("==================================== # PUBLIC # ====================================\n");
                foreach (User item in users)
                {
                    foreach (Playlist playlist in playlists)
                    {
                        if (item.ID == playlist.UserID)
                        {
                            Console.WriteLine($"ID# {playlist.ID} | Playlist: {playlist.Name} | Owner:{item.NickName}\n");
                           
                        }
                    }
                }
                Console.WriteLine("==================================== # PUBLIC # ====================================\n");
            }
        }
        public void ShowSharedPlaylists()
        {
            List<SharedPlaylists> sharedplaylists = shareRepo.GetAll(x => x.UserID == AuthenticationService.LoggedUser.ID);
            if (sharedplaylists.Count < 1)
                return;
            Console.WriteLine("\n==================== # SHARED WITH YOU # ==================\n");
            foreach (SharedPlaylists item in sharedplaylists)
            {
                Playlist playlist = playRepo.GetById(item.PlaylistID);
                User usr = userRepo.GetById(item.SharedFromUserID);

                Console.WriteLine($"#ID: {item.PlaylistID} #PlaylistName: {playlist.Name} #Owner: {usr.NickName}");
            }
            Console.WriteLine("\n==================== # SHARED WITH YOU # ==================\n");
        }
        private void AllPlaylists()
        {
            Console.Clear();
            Console.Write("*--All Playlists\n");
            List<Playlist> playlists = playRepo.GetAll(x => x.UserID == AuthenticationService.LoggedUser.ID);
            Console.WriteLine("\n*******************************************");
            foreach (Playlist item in playlists)
            {
                Console.WriteLine($"ID# {item.ID} | Playlist: {item.Name}");
                Console.WriteLine("*******************************************");
            }
            ShowSharedPlaylists();
            GetAllPublicPlaylists();
            Console.ReadKey(true);
            new UserMenuView().Show();
        }
        private void DeletePlaylist()
        {
            Console.Clear();
            Console.Write("*--Delete Playlist\n\n");
            int playlistID = ConsoleHelper.ReadIntFromKeyboard("Enter PlaylistID: ");
            Playlist play = playRepo.GetById(playlistID);
            if(play == null)
            {
                Console.Clear();
                Console.WriteLine("Playlist incorrect id ...");
            }
            else
            {
                List<PlaylistsSongs> songs = plsRepo.GetAll(x => x.PlaylistID == playlistID);
                foreach (var song in songs)
                {
                    plsRepo.Delete(song);
                    songRepo.Delete(songRepo.GetById(song.SongID));
                }
                playRepo.Delete(play);
                Console.Clear();
                Console.WriteLine("Playlist deleted ...");
                }
            Console.ReadKey(true);
            new UserMenuView().Show();
        }
        #endregion
        private UserMenuEnum RenderMenu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("      ### BlackSound ###\n");
                Console.WriteLine($"# USER: {AuthenticationService.LoggedUser.NickName} # Date:{DateTime.Now}\n");
                Console.WriteLine("1.[M]y playlist's");
                Console.WriteLine("2.[S]hare playlist");
                Console.WriteLine("3.[A]dd new playlist");
                Console.WriteLine("4.[E]dit playlist");
                Console.WriteLine("5.[D]elete playlist");
                Console.WriteLine("----------------------\n");
                Console.WriteLine("[L]og Out");
                Console.WriteLine("E[x]it");

                Console.Write("\nBlackSound> ");
                string choice = Console.ReadLine();

                switch(choice.ToUpper())
                {
                    case "M":
                        {
                            return UserMenuEnum.ViewAll;
                        }
                    case "S":
                        {
                            return UserMenuEnum.Share;
                        }
                    case "A":
                        {
                            return UserMenuEnum.Create;
                        }
                    case "E":
                        {
                            return UserMenuEnum.Edit;
                        }
                    case "D":
                        {
                            return UserMenuEnum.Delete;
                        }
                    case "L":
                        {
                            return UserMenuEnum.LogOut;
                        }
                    case "X":
                        {
                            return UserMenuEnum.Exit;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }
                }
            }
        }
        public void Show()
        {
            while(true)
            {
                UserMenuEnum choice = RenderMenu();
                while(true)
                {
                    try
                    {
                        switch(choice)
                        {
                            case UserMenuEnum.ViewAll:
                                {
                                    AllPlaylists();
                                    break;
                                }
                            case UserMenuEnum.Share:
                                {
                                    SharePlaylist();
                                    break;
                                }
                            case UserMenuEnum.Create:
                                {
                                    AddPlaylist();
                                    break;
                                }
                            case UserMenuEnum.Edit:
                                {
                                    EditPlaylist();
                                    break;
                                }
                            case UserMenuEnum.Delete:
                                {
                                    DeletePlaylist();
                                    break;
                                }
                            case UserMenuEnum.LogOut:
                                {
                                    Console.Clear();
                                    new GuestView().Show();
                                    break;
                                }
                            case UserMenuEnum.Exit:
                                {
                                    Environment.Exit(0);
                                    break;
                                }
                        }

                    }
                    catch (Exception ex)
                    {
                        Console.Clear();
                        Console.WriteLine(ex.Message);
                        Console.ReadKey(true);
                    }
                }
            }
        }
        #endregion
        #region UserPlayListMenu
        #region CRD
        private void AddSong()
        {
            Console.Clear();
            Song song = new Song();
            Console.Write("*--Add Song\n");
            Console.Write("\nSongName: ");
            song.Title = Console.ReadLine();
            Console.Write("Artist: ");
            song.ArtistName = Console.ReadLine();
            Console.Write("Year: ");
            song.Year = int.Parse(Console.ReadLine());
            songRepo.Save(song);
            Console.Clear();
            PlaylistsSongs plsong = new PlaylistsSongs();
            plsong.PlaylistID = this.play.ID;
            plsong.SongID = song.ID;
            plsRepo.Save(plsong);
            Console.Clear();
            Console.WriteLine("Song saved...");
            Console.ReadKey(true);
            new UserMenuView().ShowPlaylistMng(this.play);
        }
        private void DeleteSong()
        {
            Console.Clear();
            Console.Write("*--Delete Song\n");

            int songID = ConsoleHelper.ReadIntFromKeyboard("Delete SongID: ");

            Song song = songRepo.GetById(songID);
            PlaylistsSongs plsong = plsRepo.GetById(songID);
            if (song == null)
            {
                Console.Clear();
                Console.WriteLine("Incorrect ID !");
            }
            else
            {
                Console.Clear();
                songRepo.Delete(song);
                plsRepo.Delete(plsong);
                Console.WriteLine("Song deleted!");
            }
            Console.ReadKey(true);
            new UserMenuView().ShowPlaylistMng(this.play);

        }
        private void PlaylistSongs()
        {
            Console.Clear();
            List<PlaylistsSongs> plsongs = plsRepo.GetAll(x => x.PlaylistID == play.ID);
            Console.Write("*--All PlaylistSongs\n");
            Console.Write("*********************************************\n\n");
            Console.Write("----------------------------------------------------------------------------------\n");
            foreach (var item in plsongs)
            {
                Song song = songRepo.GetById(item.SongID);
                Console.WriteLine($"ID#{song.ID} | SongName: {song.Title} | Artist: {song.ArtistName} | Year: {song.Year}");
                Console.WriteLine("----------------------------------------------------------------------------------");
            }
            Console.ReadKey(true);
            new UserMenuView().ShowPlaylistMng(play);
        }
#endregion
        private UserPlaylistEnumMenu RenderMenuPlaylist(Playlist play)
        {
            while (true)
            {
                this.play = play;
                Console.Clear();
                Console.WriteLine("      ### BlackSound ###\n");
                Console.WriteLine($"Playlist:{play.Name}\n");
                Console.WriteLine("1.[A]dd song");
                Console.WriteLine("2.[D]elete song");
                Console.WriteLine("3.[P]laylist songs\n");
                Console.WriteLine("----------------------\n");
                Console.WriteLine("[R]eturn to profile");

                Console.Write("\nBlackSound> ");
                string choice = Console.ReadLine();

                switch(choice.ToUpper())
                {
                    case "A":
                        {
                            return UserPlaylistEnumMenu.AddSong;
                        }
                    case "D":
                        {
                            return UserPlaylistEnumMenu.DeleteSong;
                        }
                    case "P":
                        {
                            return UserPlaylistEnumMenu.AllSongs;
                        }
                    case "R":
                        {
                            return UserPlaylistEnumMenu.BackToUserMenu;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }
                }
            }
        }
        public void ShowPlaylistMng(Playlist play)
        {
            bool CheckAdmin = AuthenticationService.LoggedUser.IsAdministrator == true;
            while (true)
            {
                UserPlaylistEnumMenu choice = RenderMenuPlaylist(play);
                while(true)
                {
                    try
                    {
                        switch(choice)
                        {
                            case UserPlaylistEnumMenu.AddSong:
                                {
                                    AddSong();
                                    break;
                                }
                            case UserPlaylistEnumMenu.DeleteSong:
                                {
                                    DeleteSong();
                                    break;
                                }
                            case UserPlaylistEnumMenu.AllSongs:
                                {
                                    PlaylistSongs();
                                    break;
                                }
                            case UserPlaylistEnumMenu.BackToUserMenu:
                                {
                                    Console.Clear();
                                    if(CheckAdmin)
                                    {
                                        new AdminPanelView().ShowPM();
                                    }
                                    new UserMenuView().Show();
                                    break;
                                }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.Clear();
                        Console.WriteLine(ex.Message);
                        Console.ReadKey(true);
                    }
                }
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSound.Helpers;
using BlackSound.Repositories;
using BlackSound.Entities;
using BlackSound.Authentication;

namespace BlackSound.Views
{
    public class AdminPanelView
    {
        #region Repositories and etc.
        UserRepository userRepo = new UserRepository("blacksoundusers.txt");
        SongRepository songRepo = new SongRepository("songs.txt");
        PlaylistRepository playRepo = new PlaylistRepository("playlists.txt");
        PlaylistsSongsRepository plsRepo = new PlaylistsSongsRepository("plsongs.txt");
        SharePlaylistRepository shareRepo = new SharePlaylistRepository("sharedplaylists.txt");
        PasswordMask mask = new PasswordMask();
        Playlist play = new Playlist();
        #endregion
        #region AdminMenuMain
        private (int,int,int,int) GlobalCounter()
        {
            int userCounter = 0;
            int playlistCounter = 0;
            int songCounter = 0;
            int sharedCounter = 0;
            List<User> users = userRepo.GetAll();
            List<Playlist> playlists= playRepo.GetAll();
            List<Song> songs = songRepo.GetAll();
            List<SharedPlaylists> shared = shareRepo.GetAll();

            userCounter = users.Count;
            playlistCounter = playlists.Count;
            songCounter = songs.Count;
            sharedCounter = shared.Count;
            
            return (userCounter, playlistCounter, songCounter,sharedCounter);
        }
        private AdminMenuEnum RenderMenuMain()
        {
            var counters = GlobalCounter();
            while (true)
            {
                Console.Clear();
                Console.WriteLine("          * BLACKSOUND *");
                Console.WriteLine("        ### ADMIN PANEL ### \n");
                Console.WriteLine($"|ADMIN: {AuthenticationService.LoggedUser.NickName}|  {DateTime.Now}\n");
                Console.WriteLine("***************************************");
                Console.WriteLine("Global BlackSound Info: ");
                Console.WriteLine($"\nUsers: {counters.Item1}");
                Console.WriteLine($"Playlists/Shared: {counters.Item2} | {counters.Item4}");
                Console.WriteLine($"Songs: {counters.Item3}\n" );
                Console.WriteLine("***************************************\n");
                Console.WriteLine("1.[U]sersManager");
                Console.WriteLine("2.[P]laylistManager\n");
                Console.WriteLine("----------------------\n");
                Console.WriteLine("[L]og Out");
                Console.WriteLine("E[x]it");

                Console.Write("\nBlackSound> ");
                string choice = Console.ReadLine();

                switch(choice.ToUpper())
                {
                    case "U":
                        {
                            return AdminMenuEnum.EnterInUserManager;
                        }
                    case "P":
                        {
                            return AdminMenuEnum.EnterInPlaylistManager;
                        }
                    case "L":
                        {
                            return AdminMenuEnum.LogOut;
                        }
                    case "X":
                        {
                            return AdminMenuEnum.Exit;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }
                }
            }
            
        }
        public void Show()
        {
            while(true)
            {
                AdminMenuEnum choice = RenderMenuMain();
                    try
                    {
                        switch(choice)
                        {
                            case AdminMenuEnum.EnterInUserManager:
                                {
                                    new AdminPanelView().ShowUM();
                                    break;
                                }
                            case AdminMenuEnum.EnterInPlaylistManager:
                                {
                                    new AdminPanelView().ShowPM();
                                    break;
                                }
                            case AdminMenuEnum.LogOut:
                                {
                                    new GuestView().Show();
                                    break;
                                }
                            case AdminMenuEnum.Exit:
                                {
                                    Environment.Exit(0);
                                    break;
                                }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.Clear();
                        Console.WriteLine(ex.Message);
                        Console.ReadKey(true);
                    }
            }
        }
        #endregion
        #region AdminMenuUserManager
        #region CRUD
        private void DisplayAll()
        {
            //string pass = "".PadLeft(6, '*');
            Console.Clear();
            List<User> users = userRepo.GetAll();
            Console.WriteLine("*--All Users\n");
            Console.WriteLine("#######################################");
            foreach (User user in users)
            {
                Console.WriteLine("UserID: " + user.ID);
                Console.WriteLine("Nickname: " + user.NickName);
                Console.WriteLine("Email: " + user.email);
                Console.WriteLine("Password: " + "*********");
                Console.WriteLine("AdminFlag: " + user.IsAdministrator);
                Console.WriteLine("#######################################");
            }
            Console.ReadKey(true);
            new AdminPanelView().ShowUM();
        }
        private void Add()
        {
            Console.Clear();

            Console.WriteLine("*--Add User\n");
            User user = new User();
            Console.Write("Nickname: ");
            user.NickName = Console.ReadLine();
            Console.Write("Email: ");
            user.email = Console.ReadLine();
            Console.Write("Password: ");
            user.password = mask.ReadPassword();
            Console.Write("AdminFlag(True/False): ");
            user.IsAdministrator= Convert.ToBoolean(Console.ReadLine());
            userRepo.Save(user);
            Console.Clear();
            Console.WriteLine("User saved successfully!");
            Console.ReadKey(true);
            new AdminPanelView().ShowUM();
        }
        private void Edit()
        {
            Console.Clear();
            Console.WriteLine("*-Edit User\n");
            Console.Write("Edit UserID: ");
            int userID = Convert.ToInt32(Console.ReadLine());
            User user = userRepo.GetById(userID);
            if (user == null)
            {
                Console.Clear();
                Console.WriteLine("User not found!");
            }
            else
            {
                Console.WriteLine("#######################################");
                Console.WriteLine("[Old] Nickname: " + user.NickName);
                Console.WriteLine("[Old] Email: " + user.email);
                Console.WriteLine("[Old] Password: *************");
                Console.WriteLine("[Old] AdminFlag: " + user.IsAdministrator);
                Console.WriteLine("#######################################\n");
                Console.WriteLine("[EDIT]\n");
                Console.Write("Enter new Nickname: ");
                user.NickName = Console.ReadLine();
                Console.Write("Enter new Email: ");
                user.email = Console.ReadLine();
                Console.Write("Enter new Password: ");
                user.password = mask.ReadPassword();
                Console.Write("AdminFlag(True/False): ");
                user.IsAdministrator = Convert.ToBoolean(Console.ReadLine());
                userRepo.Save(user);
                Console.Clear();
                Console.WriteLine("User edited successfully!");
                Console.ReadKey(true);
                new AdminPanelView().ShowUM();
            }
        }
        private void Delete()
        {
            Console.Clear();
            Console.WriteLine("*--Delete User\n");
            Console.Write("Delete UserID: ");
            int userID = int.Parse(Console.ReadLine());
            User user = userRepo.GetById(userID);
            if (user == null)
            {
                Console.Clear();
                Console.WriteLine("Incorrect ID !");
            }
            else
            {
                Console.Clear();

                userRepo.Delete(user);
                Console.WriteLine("User deleted!");
            }
            Console.ReadKey(true);
            new AdminPanelView().ShowUM();
        }
        #endregion
        private AdminMenuEnumUserManager RenderMenuUsersManager()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("   # BLACKSOUND #");
                Console.WriteLine(" ### UsersManager ### \n");
                Console.WriteLine("1.[B]lackSound Users");
                Console.WriteLine("2.[C]reate new user");
                Console.WriteLine("3.[E]dit user");
                Console.WriteLine("4.[D]elete user\n");
                Console.WriteLine("----------------------\n");
                Console.WriteLine("[R]eturn to Admin Panel");

                Console.Write("\nBlackSound> ");
                string choice = Console.ReadLine();

                switch(choice.ToUpper())
                {
                    case "B":
                        {
                            return AdminMenuEnumUserManager.AllRegisteredUsers;
                        }
                    case "C":
                        {
                            return AdminMenuEnumUserManager.Create;
                        }
                    case "E":
                        {
                            return AdminMenuEnumUserManager.Edit;
                        }
                    case "D":
                        {
                            return AdminMenuEnumUserManager.Delete;
                        }
                    case "R":
                        {
                            return AdminMenuEnumUserManager.BackToAdminMenu;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }
                }
            }
        }
        public void ShowUM()
        {
            while(true)
            {
                AdminMenuEnumUserManager choice = RenderMenuUsersManager();
                    try
                    {
                        switch(choice)
                        {
                            case AdminMenuEnumUserManager.AllRegisteredUsers:
                                {
                                    DisplayAll();
                                    break;
                                }
                            case AdminMenuEnumUserManager.Create:
                                {
                                    Add();
                                    break;
                                }
                            case AdminMenuEnumUserManager.Edit:
                                {
                                    Edit();
                                    break;
                                }
                            case AdminMenuEnumUserManager.Delete:
                                {
                                    Delete();
                                    break;
                                }
                            case AdminMenuEnumUserManager.BackToAdminMenu:
                                {
                                    new AdminPanelView().Show();
                                    break;
                                }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.Clear();
                        Console.WriteLine(ex.Message);
                        Console.ReadKey(true);
                    }
            }
        }
        #endregion
        #region AdminMenuPlaylistManager
        #region CRUD
        private void GetAllUsersPlaylists()
        {
            Console.Clear();
            Console.WriteLine("*--All UsersPlaylist's\n");
            List <User> users = userRepo.GetAll();
            List<Playlist> playlists = playRepo.GetAll();
            Console.WriteLine("########################################");
            foreach (User item in users)
            {
                Console.WriteLine($"Owner:{item.NickName}");
                foreach (Playlist playlist in playlists)
                {
                    if (item.ID == playlist.UserID)
                    {
                        Console.WriteLine($"PlaylistID: {playlist.ID} | {playlist.Name}");
                    }
                }
                Console.WriteLine("########################################");
            }
            new UserMenuView().ShowSharedPlaylists();
            new UserMenuView().GetAllPublicPlaylists();
            Console.ReadKey(true);
            new AdminPanelView().ShowPM();
        }
        private void AddPlaylist()
        {
            Console.Clear();
            Console.Write("--Add Playlist\n");
            Playlist play = new Playlist();
            play.UserID = AuthenticationService.LoggedUser.ID;
            Console.Write("\nPlaylistName: ");
            play.Name = Console.ReadLine();
            Console.Write("Description: ");
            play.Description = Console.ReadLine();
            Console.Write("Public(True/False): ");
            play.IsPublic = Convert.ToBoolean(Console.ReadLine());
            playRepo.Save(play);
            Console.Clear();
            Console.WriteLine("Playlist saved ...");
            Console.ReadKey(true);
            new AdminPanelView().ShowPM();
        }
        private void EditPlaylist()
        {
            Console.Clear();
            Console.Write("*-- Edit Playlist\n");
            Console.Write("\nChoose PlaylistID: ");
            int playlistID = int.Parse(Console.ReadLine());
            Playlist play = playRepo.GetById(playlistID);
            if (play == null)
            {
                Console.Clear();
                Console.Write("Playlist not found!");
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Playlist found ...");
                Console.ReadKey(true);
                new UserMenuView().ShowPlaylistMng(play);
            }
        }
        private void DeletePlaylist()
        {
            Console.Clear();
            Console.Write("*--Delete Playlist\n\n");
            int playlistID = ConsoleHelper.ReadIntFromKeyboard("Enter PlaylistID: ");
            Playlist play = playRepo.GetById(playlistID);
            if (play == null)
            {
                Console.Clear();
                Console.WriteLine("Playlist incorrect id ...");
            }
            else
            {
                List<PlaylistsSongs> songs = plsRepo.GetAll(x => x.PlaylistID == playlistID);
                foreach (var song in songs)
                {
                    plsRepo.Delete(song);
                    songRepo.Delete(songRepo.GetById(song.SongID));
                }
                playRepo.Delete(play);
                Console.Clear();
                Console.WriteLine("Playlist deleted ...");
            }
            Console.ReadKey(true);
            new AdminPanelView().ShowPM();
        }
        #endregion
        
        private AdminMenuEnumPlaylistManager RenderMenuPlaylistManager()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("     # BLACKSOUND #");
                Console.WriteLine(" ### PlaylistManager ### \n");
                Console.WriteLine("1.[B]lackSound all Playlist's");
                Console.WriteLine("2.[A]dd new playlist");
                Console.WriteLine("3.[E]dit playlist");
                Console.WriteLine("4.[D]elete playlist\n");
                Console.WriteLine("----------------------\n");
                Console.WriteLine("[R]eturn to Admin Panel");

                Console.Write("\nBlackSound> ");
                string choice = Console.ReadLine();

                switch(choice.ToUpper())
                {
                    case "B":
                        {
                            return AdminMenuEnumPlaylistManager.AllUsersPlaylists;
                        }
                    case "A":
                        {
                            return AdminMenuEnumPlaylistManager.AddPlaylist;
                        }
                    case "E":
                        {
                            return AdminMenuEnumPlaylistManager.EditPlaylist;
                        }
                    case "D":
                        {
                            return AdminMenuEnumPlaylistManager.DeletePlaylist;
                        }
                    case "R":
                        {
                            return AdminMenuEnumPlaylistManager.BackToAdminMenu;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }
                }
            }
        }
        public void ShowPM()
        {
            while(true)
            {
                AdminMenuEnumPlaylistManager choice = RenderMenuPlaylistManager();
                    try
                    {
                        switch(choice)
                        {
                            case AdminMenuEnumPlaylistManager.AllUsersPlaylists:
                                {
                                    GetAllUsersPlaylists();
                                    break;
                                }
                            case AdminMenuEnumPlaylistManager.AddPlaylist:
                                {
                                    AddPlaylist();
                                    break;
                                }
                            case AdminMenuEnumPlaylistManager.EditPlaylist:
                                {
                                    EditPlaylist();
                                    break;
                                }
                            case AdminMenuEnumPlaylistManager.DeletePlaylist:
                                {
                                    DeletePlaylist();
                                    break;
                                }
                            case AdminMenuEnumPlaylistManager.BackToAdminMenu:
                                {
                                    new AdminPanelView().Show();
                                    break;
                                }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.Clear();
                        Console.WriteLine(ex.Message);
                        Console.ReadKey(true);
                    }
            }
        }
        #endregion
    }
}




﻿using BlackSound.Helpers;
using BlackSound.Authentication;
using BlackSound.Repositories;
using BlackSound.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Views
{
    public class LoginView
    {
        #region Login
        
        public void Show()
        {
            while(true)
            {
                Console.Clear();
                Console.WriteLine("BlackSound> [LOGIN]\n");
                Console.Write("Email: ");
                string email = Console.ReadLine();
                Console.Write("Password: ");
                PasswordMask pm = new PasswordMask();
                string password = pm.ReadPassword();
                if (email == string.Empty || password == string.Empty)
                {
                    Console.Clear();
                    throw new NullReferenceException("Your input cannot be null ...");
                }
                AuthenticationService.AuthenticateUser(email, password);
                if(AuthenticationService.LoggedUser==null)
                {
                    Console.Clear();
                    throw new NullReferenceException("This user doesen't exists ...");
                }
                bool CheckAdmin = AuthenticationService.LoggedUser.IsAdministrator == true;
                if (CheckAdmin)
                {
                    Console.Clear();
                    Console.WriteLine("Welcome \nADMIN: " + AuthenticationService.LoggedUser.NickName);
                    Console.ReadKey(true);
                    new AdminPanelView().Show();
                    break;
                }
                if (AuthenticationService.LoggedUser != null)
                {
                    Console.Clear();
                    Console.WriteLine("Welcome " + AuthenticationService.LoggedUser.NickName);
                    Console.ReadKey(true);
                    new UserMenuView().Show();
                    break;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Invalid entry");
                    Console.ReadKey(true);
                }
            }
        }
        #endregion
    }
}

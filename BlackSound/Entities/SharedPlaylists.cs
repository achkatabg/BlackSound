﻿using BlackSound.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Entities
{
    public class SharedPlaylists : IEntity
    {
        public int ID { get; set; }
        public int SharedFromUserID { get; set; }
        public int PlaylistID { get; set; }
        public int UserID { get; set; }
    }
}
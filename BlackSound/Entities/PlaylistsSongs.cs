﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSound.Helpers;

namespace BlackSound.Entities
{
   public class PlaylistsSongs : IEntity
    {
        public int ID { get; set; }
        public int PlaylistID { get; set; }
        public int SongID { get; set; }

    }
}

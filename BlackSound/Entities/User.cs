﻿using BlackSound.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Entities
{
   public class User : IEntity
    {
        public int ID { get; set; }
        public string NickName { get; set;}
        public string email { get; set; }
        public string password { get; set; }
        public bool IsAdministrator { get; set; }
    }
}

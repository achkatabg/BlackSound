﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Helpers
{
    public enum AdminMenuEnum
    {
        EnterInUserManager = 1,
        EnterInPlaylistManager = 2,
        LogOut = 3,
        Exit = 4
    }
    public enum AdminMenuEnumUserManager
    {
        AllRegisteredUsers = 1,
        Create = 2,
        Edit = 3,
        Delete = 4,
        BackToAdminMenu = 5
    }
    public enum AdminMenuEnumPlaylistManager
    {
        AllUsersPlaylists = 1,
        AddPlaylist = 2,
        EditPlaylist = 3,
        DeletePlaylist = 4,
        BackToAdminMenu = 5
    }
    public enum UserMenuEnum
    {
        ViewAll = 1,
        Share = 2,
        Create = 3,
        Edit = 4,
        Delete = 5,
        LogOut = 6,
        Exit = 7
    }
    public enum UserPlaylistEnumMenu
    {
        AddSong = 1,
        DeleteSong = 2,
        AllSongs = 3,
        BackToUserMenu = 4
    }
    public enum GuestMenuEnum
    {
        Login = 1,
        Register = 2,
        Exit = 3
    }
}

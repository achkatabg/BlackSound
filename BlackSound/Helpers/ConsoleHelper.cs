﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Helpers
{
    public class ConsoleHelper
    {
        public static int ReadIntFromKeyboard(string promptText = null)
        {
            int number;
            bool isValidNumber = false;
            do
            {
                if (promptText != null)
                {
                    Console.Write(promptText);
                }

                string str = Console.ReadLine();
                isValidNumber = int.TryParse(str, out number);
                if (isValidNumber == false)
                {
                    Console.WriteLine("Enter a valid number!");
                    Console.ReadKey(true);
                    Console.Clear();
                }
            } while (isValidNumber == false);
            return number;
        }
    }
}

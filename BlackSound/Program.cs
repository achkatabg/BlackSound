﻿using BlackSound.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound
{
    class Program
    {
        static void Main(string[] args)
        {
            new GuestView().Show();
            Console.ReadKey(true);
        }
    }
}

﻿using BlackSound.Entities;
using BlackSound.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Authentication
{
    public static class AuthenticationService
    {
        public static User LoggedUser { get; private set; }

        public static void AuthenticateUser(string email, string password)
        {
            UserRepository userRepo = new UserRepository("blacksoundusers.txt");
            AuthenticationService.LoggedUser = userRepo.GetByEmailAndPassword(email, password); 
        }
    }
}
